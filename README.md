# can_zoe_msgs

Package ROS1 that contain ros messages use to communicate with the CAN bus of the Zoe1.

## Message List

| **Name**               | **Use**                                               |
|:----------------------:|:-----------------------------------------------------:|
| ActivationCondition    | Check the condition of robotization                   |
| BattInfo               | Battery information                                   |
| CmdAcceleration        | Order of Acceleration                                 |
| CmdBrake               | Order of Braking                                      |
| CmdOptions             | Order to control the blinker,light and horn           |
| CmdPRND                | Order for the gearbox                                 |
| Frame                  | CAN message                                           |
| IMU                    | IMU sensor message                                    |
| Odom                   | Odom sensor message                                   |
| PRNDCmdInfo            | gear box information                                  |
| RobotizationActivation | state of control activation                           |
| SelectorAInfo          | state of controlpannel Led and buzzer                 |
| SelectorBInfo          | state of switch led and buzzer on the controlpannel   |
| Speed                  | Speed information                                     |
| SpeedControl           | Order for speed control                               |
| Steering               | Steering informtion                                   |
| SteeringCmdInfo        | Steering control information                          |
| TorqueInfo             | Torque information                                    |

# Import messages

## Python

```python
from can_zoe_msgs.msg import CmdSteering
from can_zoe_msgs.msg import CmdAcceleration
from can_zoe_msgs.msg import CmdBrake
from can_zoe_msgs.msg import Speed
from can_zoe_msgs.msg import Steering

if __name__ == '__main__':
  #var init
  acc = CmdAcceleration()
  acc.can_publish = True
  brk = CmdBrake()
  brk.can_publish = True
  steer = CmdSteering()
  steer.can_publish = True
  steer.cooperative_mode = 0
  steer.rotation_speed = 100.0
```

## C++

```cpp
#include "can_zoe_msgs/CmdAcceleration.h"
#include "can_zoe_msgs/CmdBrake.h"
#include "can_zoe_msgs/CmdSteering.h"
#include "can_zoe_msgs/CmdPRND.h"
#include "can_zoe_msgs/SpeedControl.h"

int main (){
  can_zoe_msgs::CmdAcceleration cmdAccel;
  can_zoe_msgs::CmdBrake cmdBrake;

  cmdAccel.acceleration = 0.0;
}
}
```
