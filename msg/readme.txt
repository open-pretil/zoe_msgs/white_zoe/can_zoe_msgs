
Checksum Id
Counter Id

mode auto: PRND, EPS (DAE)

activated conditions :
0 "valid" 1 "invalid"
CAN intersystem

IMU :

Longitudinal acceleration [-10...2.75 m/s2]
Lateral acceleration [-131.072...131.068 g]

Odometry :
[0...65535 1top/cm]

